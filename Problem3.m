function [] = Problem3
%ProjectEuler/Octave/Problem3.m
%Matthew Ellison
% Created: 03-28-19
%Modified: 10-28-20
%The largest prime factor of 600851475143
%{
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


	%Setup your variables
	number = 600851475143;	%The number we are trying to find the greatest prime factor of
	factors = [];	%For the list of factors of number

	%Get the prime numbers up to sqrt(number). If it is not prime there must be a value <= sqrt
	primeNums = primes(sqrt(number));

	%Start the timer
	startTime = clock();

	factors = factor(600851475143);

	%Stop the timer
	endTime = clock();

	%Print the results
	printf("The largest prime factor of 600851475143 is %d\n", max(factors))
	printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))
end

%{
Results:
The largest prime factor of 600851475143 is 6857
It took 0.005714 seconds to run this algorithm
%}
