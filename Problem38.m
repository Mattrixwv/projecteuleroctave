function [] = Problem38()
%ProjectEuler/Octave/Problem38.m
%Matthew Ellison
% Created: 10-12-21
%Modified: 10-12-21
%What is the largest 1-9 pandigital number that can be formed as the concatenated product of an integer with 1, 2, ... n where n > 1
%{
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Start the timer
startTime = clock();


%Setup the variables
HIGHEST_POSSIBLE_NUM = 9999;	%The highest number that needs to be checked for a 1-9 pandigital
largestNum = 0;	%The number passed to the executeFormula function that returns the largest pandigital
pandigital = 0;	%The largest pandigital number found

%Loop from 1 -> HIGHEST_POSSIBLE_NUM checking for pandigitals
for cnt = 1 : HIGHEST_POSSIBLE_NUM
	%Get the string from the formula
	numStr = executeFormula(cnt);
	panNum = str2num(numStr);
	%If the number is pandigital save it as the highest number
	if(isPandigital(numStr) && (panNum > pandigital))
		largestNum = cnt;
		pandigital = panNum;
	end
end


%Stop the timer
endTime = clock();

%Print the results
printf("The largest appened product pandigital is %d\n", pandigital)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

function [numStr] = executeFormula(num)
	%Turn the current number into a string
	numStr = num2str(num);
	numStr = strcat(numStr, num2str(num * 2));

	%Multiply the number and append the product to the string until you have one long enough
	cnt = 3;
	while(size(numStr)(2) < 9)
		numStr = strcat(numStr, num2str(num * cnt));
		cnt += 1;
	end
end

function [isPan] = isPandigital(str)
	top = 9;
	bottom = 1;
	isPan = false;
	%Return false if top < bottom
	if(top < bottom)
		return;
	end

	%Return false if the wrong number of characters are in the string
	if(size(str)(2) != (top - bottom + 1))
		return
	end

	%Make sure that all of the needed characters are in the string exactly one time
	for cnt = bottom : top
		if(size(strfind(str, num2str(cnt)))(2) != 1)
			return;
		end
	end

	%If the function has reched this part it has passed all of the falsifying tests
	isPan = true;
end


%{
Results:
The largest appened product pandigital is 932718654
It took 13.246925 seconds to run this algorithm
%}
