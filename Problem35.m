function [] = Problem35()
%ProjectEuler/ProjectEulerOctave/Problem35.lua
%Matthew Ellison
% Created: 06-05-21
%Modified: 06-05-21
%Find the sum of all numbers which are equal to the sum of the factorial of their digits
%{
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}

%Setup the variables
MAX_NUM = 999999;
circularPrimes = [];

%Start the timer
startTime = clock();

%Get all primes under 1,000,000
primeList = primes(MAX_NUM);
%Go through all primes, get all their rotations, and check if those numbers are also prime
for cnt = 1 : size(primeList)(2)
	prime = primeList(cnt)
	allRotationsPrime = true;
	%Get all of the rotations of the pirme and see if they are also prime
	rotations = getRotations(num2str(prime));
	for rotCnt = 1 : size(rotations)(2)
		rotation = rotations(rotCnt);
		p = rotation;
		if(~isprime(p))
			allRotationsPrime = false;
			break;
		end
	end
	%If all rotations are prime add it to the list of circular primes
	if(allRotationsPrime)
		circularPrimes(end + 1) = prime;
	end
end

%Stop the timer
endTime = clock();

%Print the results
printf("The number of all circular prime numbers under %d is %d\n", MAX_NUM, size(circularPrimes)(2));
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

%Returns a list of all rotations of a string passed to it
function [rotations] = getRotations(str)
	rotations = [];
	rotations(end + 1) = str2num(str);
	for cnt = 2 : size(str)(2)
		temp = [substr(str, 2, size(str)(2) - 1) str(1)(1)];
		num = str2num(temp);
		rotations(end + 1) = num;
		str = temp;
	end
end

function [found] = isFound(array, key)
	found = false;
	for location = 1 : size(array)(2)
		if(key == array(location))
			found = true;
			return;
		end
	end
end

%{
Results:
The number of all circular prime numbers under 999999 is 55
It took 712.060379 seconds to run this algorithm
%}
