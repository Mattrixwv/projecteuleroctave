function [] = Problem24()
%ProjectEuler/Octave/Problem24.m
%Matthew Ellison
% Created: 03-25-19
%Modified: 03-28-19
%What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


NEEDED_PERM = 1000000;	%The number of the permutation that you need


%Setup the variables
nums = "0123456789";

%Start the time
startTime = clock();

%Get all permutations of the string
permutations = getPermutations(nums);

%Stop the timer
endTime = clock();

%Print the results
printf("The 1 millionth permutation is %s\n", permutations{NEEDED_PERM})
printf("It took %f second to run this algorithm\n", etime(endTime, startTime))


end	%Problem24()

function [perms] = getPermutations(master, num = 1)
	perms = {};
	%Check if the number is out of bounds
	if((num > size(master)(2)) || (num <= 0))
		%Do nothing and return and empty list
		perms;
	%If this is the last possible recurse just return the current string
	elseif(num == size(master)(2))
		perms(end + 1) = master;
	%If there are more possible recurses, recurse with the current permutations
	else
		temp = getPermutations(master, num + 1);
		perms = {perms{:}, temp{:}};
		%You need to swap the current letter with every possible letter after it
		%The ones needed to swap before will happen automatically when the function recurses
		for cnt = 1 : (size(master)(2) - num)
			%Swap two elements
			temp = master(num);
			master(num) = master(num + cnt);
			master(num + cnt) = temp;
			%Get the permutations after swapping two elements
			temp = getPermutations(master, num + 1);
			perms = {perms{:}, temp{:}};
			%Swap the elements back
			temp = master(num);
			master(num) = master(num + cnt);
			master(num + cnt) = temp;
		end
	end

	%The array is not necessarily in alpha-numeric order. So if this is the full array sort it before returning
	if(num == 1)
		perms = sort(perms);
	end
end	%getPermutations()

%{
Results:
The 1 millionth permutation is 2783915460
It took 433.922920 second to run this algorithm
%}
