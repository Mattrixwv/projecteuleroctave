%ProjectEuler/Octave/Problem8.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?
%{
73167176531330624919225119674426574742355349194934
96983520312774506326239578318016984801869478851843
85861560789112949495459501737958331952853208805511
12540698747158523863050715693290963295227443043557
66896648950445244523161731856403098711121722383113
62229893423380308135336276614282806444486645238749
30358907296290491560440772390713810515859307960866
70172427121883998797908792274921901699720888093776
65727333001053367881220235421809751254540594752243
52584907711670556013604839586446706324415722155397
53697817977846174064955149290862569321978468622482
83972241375657056057490261407972968652414535100474
82166370484403199890008895243450658541227588666881
16427171479924442928230863465674813919123162824586
17866458359124566529476545682848912883142607690042
24219022671055626321111109370544217506941658960408
07198403850962455444362981230987879927244284909188
84580156166097919133875499200524063689912560717606
05886116467109405077541002256983155200055935729725
71636269561882670428252483600823257530420752963450
%}
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup your variables
%The string of the number
number = '7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450';
counter = 1;	%Location of the first digit in the series
productNumbers = [''];	%The numbers in the current product
greatestProduct = [];	%The numbers in the greatest product

%Start the timer
startTime = clock();

%Loop through the string until every element has been tested
while((counter + 12) < size(number)(2))
	innerCounter = 0;
	productNumbers = [''];	%Clear the variable
	while(innerCounter < 13)
		%Octave throws an error if you don't take this round about way of adding the characters to the array
		tempChar = '';	%Throw away variable
		tempChar = [number(counter + innerCounter), ' '];	%Add the next number to what you already have and add a space at the end
		productNumbers = [productNumbers, tempChar];
		++innerCounter;
	end
	productNumbers = str2num(productNumbers);	%Convert the characters to numbers
	%Check whether the current product is greater than the current greatest product
	if(prod(productNumbers) > prod(greatestProduct))
		greatestProduct = productNumbers;
	end
	++counter;
end

%Stop the timer
endTime = clock();

%Print the results
printf("The largest product of 13 adjacent digits in the number is %d\n", prod(greatestProduct))
printf("The numbers are: %d %d %d %d %d %d %d %d %d %d %d %d %d\n", greatestProduct(1), greatestProduct(2), greatestProduct(3), greatestProduct(4), greatestProduct(5), greatestProduct(6), greatestProduct(7), greatestProduct(8), greatestProduct(9), greatestProduct(10), greatestProduct(11), greatestProduct(12), greatestProduct(13))
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup your variables
clear number;
clear counter;
clear productNumbers;
clear greatestProduct;
clear tempChar;
clear innerCounter;
clear startTime;
clear endTime;
clear ans;

%{
Results:
The largest product of 13 adjacent digits in the number is 23514624000
The numbers are: 5 5 7 6 6 8 9 6 6 4 8 9 5
It took 0.431618 seconds to run this algorithm
%}
