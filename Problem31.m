function [] = Problem31()
%ProjectEuler/Octave/Problem31.m
%Matthew Ellison
% Created: 06-19-20
%Modified: 06-19-20
%How many different ways can £2 be made using any number of coins?
%{
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup the variables
desiredValue = 200;
permutations = 0;

%Start the timer
startTime = clock();

%Start with 200p and remove the necessary coins with each loop
pound2 = desiredValue;
while(pound2 >= 0)
	pound1 = pound2;
	while(pound1 >= 0)
		pence50 = pound1;
		while(pence50 >= 0)
			pence20 = pence50;
			while(pence20 >= 0)
				pence10 = pence20;
				while(pence10 >= 0)
					pence5 = pence10;
					while(pence5 >= 0)
						pence2 = pence5;
						while(pence2 >= 0)
							permutations += 1;
							pence2 -= 2;
						end
						pence5 -= 5;
					end
					pence10 -= 10;
				end
				pence20 -= 20;
			end
			pence50 -= 50;
		end
		pound1 -= 100;
	end
	pound2 -= 200;
end

%Stop the timer
endTime = clock();

%Print the results
printf("There are %d ways to make 2 pounds with the given denominations of coins\n", permutations);
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

%{
Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took 0.122734 seconds to run this algorithm
%}
