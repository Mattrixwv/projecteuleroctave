%ProjectEuler/Octave/Problem11.m
%Matthew Ellison
% Created:
%Modified: 03-28-19
%What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?
%{
08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48
%}
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Create your variables
grid = [08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08;
		49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00;
		81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65;
		52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91;
		22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80;
		24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50;
		32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70;
		67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21;
		24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72;
		21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95;
		78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92;
		16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57;
		86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58;
		19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40;
		04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66;
		88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69;
		04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36;
		20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16;
		20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54;
		01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48];
currentLocation = [1, 1];	%The location you are checking now
currentProduct = [0 0 0 0];	%The product you are currently looking at
greatestProduct = [0 0 0 0];	%The greatest product of values you have found
finished = false;

%Start the timer
startTime = clock();

%Loop until you reach the last element
while(~finished)
	left = false;
	right = false;
	down = false;

	%Check which directions you can go
	%When moving you will be moving 4 locations
	if((currentLocation(2) - 3) >= 1)
		left = true;
	end
	if((currentLocation(2) + 3) <= size(grid)(2))
		right = true;
	end
	if((currentLocation(1) + 3) <= size(grid)(1))
		down = true;
	end

	%Check the possible directions and check against greatest
	%Right
	if(right)
		currentProduct = grid(currentLocation(1), currentLocation(2):(currentLocation(2) + 3));
		%If the current numbers' product is greater than the greatest product so far, replace it
		if(prod(currentProduct) > prod(greatestProduct))
			greatestProduct = currentProduct;
		end
	end
	%Down
	if(down)
		currentProduct = grid(currentLocation(1):(currentLocation(1) + 3), currentLocation(2));
		%If the current numbers' product is greater than the greatest product so far, replace it
		if(prod(currentProduct) > prod(greatestProduct))
			greatestProduct = currentProduct;
		end
	end
	%LeftDown
	if(left && down)
		currentProduct(1) = grid(currentLocation(1), currentLocation(2));
		currentProduct(2) =	grid(currentLocation(1) + 1,currentLocation(2) - 1);
		currentProduct(3) = grid(currentLocation(1) + 2,currentLocation(2) - 2);
		currentProduct(4) = grid(currentLocation(1) + 3,currentLocation(2) - 3);
		%If the current numbers' product is greater than the greatest product so far, replace it
		if(prod(currentProduct) > prod(greatestProduct))
			greatestProduct = currentProduct;
		end
	end
	%RightDown
	if(right && down)
		currentProduct(1) = grid(currentLocation(1), currentLocation(2));
		currentProduct(2) =	grid(currentLocation(1) + 1,currentLocation(2) + 1);
		currentProduct(3) = grid(currentLocation(1) + 2,currentLocation(2) + 2);
		currentProduct(4) = grid(currentLocation(1) + 3,currentLocation(2) + 3);
		%If the current numbers' product is greater than the greatest product so far, replace it
		if(prod(currentProduct) > prod(greatestProduct))
			greatestProduct = currentProduct;
		end
	end

	%Move to the next column
	++currentLocation(2);
	%If you have moved too far in the columns move back to the beginning and to the next row
	if(currentLocation(2) > size(grid)(2))
		currentLocation(2) = 1;
		++currentLocation(1);
	end
	%If the row is currently greater than what is available you have traversed the list
	if(currentLocation(1) > size(grid)(1))
		finished = true;
	end
end
greatestProduct = reshape(greatestProduct, 1, 4);	%For some reason it is coming out a 4X1 instead of 1X4

%Stop the timer
endTime = clock();

%Print the result
printf("The greatest product of 3 numbers in a line is %d\n", prod(greatestProduct))
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup the variables
clear down;
clear right;
clear left;
clear finished;
clear startTime;
clear endTime;
clear grid;
clear greatestProduct;
clear currentLocation;
clear currentProduct;
clear ans;

%{
Results:
The greatest product of 3 numbers in a line is 70600674
It took 0.081856 seconds to run this algorithm
%}
