%ProjectEuler/Octave/Problem6.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Start the timer
startTime = clock();

%Setup your variables
nums = [1:100];
squares = nums.^2;	%Square every number in the list nums
sumOfSquares = sum(squares);	%Get the sum of all the elements in the list squares
squareOfSums = sum(nums)^2;		%Get the sum of all the elements in the list nums and square the answer
value = abs(squareOfSums - sumOfSquares);	%Get the difference of the 2 numbers
%This could all be done on one line, but this is less confusing

%Stop the timer
endTime = clock();

%Print the results
printf("The difference between the sum of the squares and the square of the sum of the numbers 1-100 is %d\n", value)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup your variables
clear nums;
clear squares;
clear sumOfSquares;
clear squareOfSums;
clear value;
clear startTime;
clear endTime;

%{
Results:
The difference between the sum of the squares and the square of the sum of the numbers 1-100 is 25164150
It took 0.000137 seconds to run this algorithm
%}
