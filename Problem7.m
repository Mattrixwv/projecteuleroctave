%ProjectEuler/Octave/Problem7.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%What is the 10001th prime number?
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup the variables
counter = 1000;
primeList = [];

%Start the timer
startTime = clock();

%Cycle through the prime numbers until you get the correct number
while(size(primeList)(2) < 10001)
	primeList = primes(counter);
	counter += 1000;
end

%Stop the timer
endTime = clock();

%Print the results
printf("The 10001st prime number is %d\n", primeList(10001))
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup the variables
clear counter;
clear primeList;
clear startTime;
clear endTime;

%{
Results:
The 10001st prime number is 104743
It took 0.107124 seconds to run this algorithm
%}
