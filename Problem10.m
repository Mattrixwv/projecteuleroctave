%ProjectEuler/Octave/Problem10.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%Find the sum of all the primes below two million.
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Start the timer
startTime = clock();

%Find the number
num = sum(primes(2000000));

%Stop the timer
endTime = clock();

%Print the results
printf("The sum of all the prime numbers less than 2000000 is %d\n", num)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup your variables
clear num;
clear startTime;
clear endTime;
clear timeToRun;

%{
Results:
The sum of all the prime numbers less than 2000000 is 142913828922
It took 0.012894 seconds to run this algorithm
%}
