function [] = Problem1()
%ProjectEuler/Octave/Problem1.m
%Matthew Ellison
% Created: 03-28-19
%Modified: 10-26-20
%What is the sum of all the multiples of 3 or 5 that are less than 1000
%{
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


	%Setup your variables
	fullSum = 0;	%To hold the sum of all the numbers
	numbers = 0;	%To hold all of the numbers
	counter = 0;	%The number. It must stay below 1000

	%Start the timer
	startTime = clock();

	%Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
	sumOfMultiples = sumOfProgression(3) + sumOfProgression(5) - sumOfProgression(3 * 5);

	%Stop the timer
	endTime = clock();

	%Print the results
	printf("The sum of all the numbers less than 1000 that is divisibly by 3 or 5 is: %d\n", sumOfMultiples)
	printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end %End of Problem1()

%Gets the sum of the progression of the multiple
function [sum] = sumOfProgression(multiple)
	numTerms = floor(999 / multiple);	%This gets the number of multiples of a particular number that is < MAX_NUMBER
	%The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
	sum = ((numTerms / 2) * (multiple + (numTerms * multiple)));
end %End of sumOfProgression

%{
Results:
The sum of all the numbers less than 1000 that is divisibly by 3 or 5 is: 233168
It took 0.014717 seconds to run this algorithm
%}
