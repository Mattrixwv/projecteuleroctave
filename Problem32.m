function [] = Problem32()
%ProjectEuler/Octave/Problem32.m
%Matthew Ellison
% Created: 07-28-20
%Modified: 07-28-20
%Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
%{
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Start the timer
startTime = clock();

%Setup the variables
topMultiplicand = 99;	%The largest multiplicand to check
topMultiplier = 4999;	%The largest multiplier to check
listOfProducts = {};	%The list of unique products that are 1-9 pandigital
sumOfPandigitals = 0;	%The sum of the products of the pandigital numbers

%Create the multiplicand and start working your way up
for multiplicand = 1 : topMultiplicand
	%Run through all possible multipliers
	for multiplier = multiplicand : topMultiplier
		currentProductSet = [multiplicand, multiplier];
		%If the product is too long move on to the next possible number
		if(size(num2str(getNumString(currentProductSet)))(2) > 9)
			break
		end
		%If the current number is a pandigital that doesn't already exist in the list add it
		if(isPandigital(currentProductSet))
			if(~productInTable(listOfProducts, currentProductSet))
				listOfProducts(end + 1) = currentProductSet;
			end
		end
	end
end

%Get the sum of the products of the pandigitals
for prod = 1 : size(listOfProducts)(2)
	sumOfPandigitals += getProduct(listOfProducts{prod});
end


%Stop the timer
endTime = clock();

%Print the results
printf("There are %d unique 1-9 pandigitals\n", size(listOfProducts)(2))
printf("The sum of the products of these pandigitals is %d\n", sumOfPandigitals)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

function [isPan] = isPandigital(currentSet)
	%Get the number out of the object and put them into a string
	numberString = getNumString(currentSet);
	%Make sure the string is the correct length
	if(size(numberString)(2) != 9)
		isPan = false;
		return;
	end
	%Make sure there is exactly one of this number contained in the string
	for panNumber = 1 : 9
		if(size(strfind(numberString, num2str(panNumber)))(2) != 1)
			isPan = false;
			return;
		end
	end
	isPan = true;
end

function [prod] = getProduct(currentSet)
	prod = (currentSet(1) * currentSet(2));
end

function [numString] = getNumString(currentSet)
	numString = strcat(num2str(currentSet(1)), num2str(currentSet(2)), num2str(currentSet(1) * currentSet(2)));
end

function [inTable] = productInTable(startTable, element)
	inTable = false;
	for cnt = 1 : size(startTable)(2)
		if(getProduct(startTable{cnt}) == getProduct(element))
			inTable = true;
			return;
		end
	end
end

%{
Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took 174.908974 seconds to run this algorithm
%}
