function numOfRoutes = Problem15()
%ProjectEuler/Octave/Problem15.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup your variables
gridLength = 20;
gridHeight = 20;
numOfRoutes = 0;
currentX = 0;
currentY = 0;

%Start the timer
startTime = clock();

%Get the number of routes from top left to bottom right
numOfRoutes = movement(currentX, currentY, gridLength, gridHeight);

%Stop the timer
endTime = clock();

%Print the results
printf("The number of routes from top left to bottom right is %d\n", numOfRoutes)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Cleanup your variables
clear gridLength;
clear gridHeight;
clear numOfRoutes;
clear currentX;
clear currentY;
clear startTime;
clear endTime;

end %End of Problem15()

%Simulates moving along the grid
%Recurses moving right first, then down
function num = movement(currentX, currentY, gridLength, gridHeight)
	num = 0;
	%See if it is at the end of the grid
	if((currentX == gridLength) && (currentY == gridHeight))
		num = 1;
	else
		%If it's not move right first, then move down, if possible
		if(currentX < gridLength)
			num += movement(currentX + 1, currentY, gridLength, gridHeight);
		end
		if(currentY < gridHeight)
			num += movement(currentX, currentY + 1, gridLength, gridHeight);
		end
	end
end

%{
Results:
///This follows the same idea as my cpp program but I got tired of waiting for it to finish after two hours.
///It should work, but is still untested
%}
