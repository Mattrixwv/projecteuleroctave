function sumOfLetters = Problem17()	%In order to have other functions the main part had to be a function as well
%ProjectEuler/Octave/Problem17.m
%Matthew Ellison
% Created: 02-05-19
%Modified: 03-28-19
%If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup your variables
sumOfLetters = 0;
startNum = 1;
stopNum = 1000;

%Start the timer
startTime = clock();

%Start with 1 and increment
currentNum = startNum;
for(currentNum = startNum:stopNum)
	%Pass the number to a function that will create a string for the number
	currentNumString = getStringFromNum(currentNum);
	%Pass the string to a function that will count the number of letters in a string, ignoring whitespace and punctuation and add the amount to the running tally
	sumOfLetters += getNumberChars(currentNumString);
end

%Stop the timer
endTime = clock();

%Print the results
printf("The sum of all printed number from %d-%d is %d\n", startNum, stopNum, sumOfLetters)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

%Clean up the variables
clear sumOfLetters;
clear startNum;
clear stopNum;
clear startTime;
clear endTime;

end	%End of Problem17()

%This function returns the number of characters in a string, ignoring whitespace and punctuation
function sumOfLetters = getNumberChars(number)
	sumOfLetters = 0;
	%Start at location 0 and count the number of letters
	for location = 1:size(number)(2)
		tempString = substr(number, location, 1);
		if(isalpha(tempString))
			++sumOfLetters;
		end
	end
end

%This function creates a string from a number
%It only works for numbers -1,000,000 < num < 1,000,000
function numberString = getStringFromNum(number)
	numberString = "";
	%Starting with the largest digit create a string based on the number passed in
	%Check for negative
	if(number < 0)
		numberString = strcat(numberString, "negative ");
	end

	%Check if the number is zero
	if(number == 0)
		numberString = strcat(numberString, "zero");
	end

	%Start with the thousands place
	if((number / 1000) >= 1)
		numberString = strcat(numberString, getStringFromNum(floor(number / 1000)));
		numberString = strcat(numberString, " thousand");
		number -= (floor(number / 1000) * 1000);
	end

	%Check for hundreds place
	if((number / 100) >= 1)
		numberString = strcat(numberString, getStringFromNum(floor(number / 100)));
		numberString = strcat(numberString, " hundred");
		number -= (floor(number / 100) * 100);
	end

	%Insert an and if there is need
	if((size(numberString)(2) != 0) && (number > 0))
		numberString = strcat(numberString, " and ");
	end

	%Check for tens place
	if((number / 10) >= 2)
		%For the tens you need to do something special
		tensPlace = floor(number / 10);
		if(tensPlace == 9)
			numberString = strcat(numberString, "ninety");
		elseif(tensPlace == 8)
			numberString = strcat(numberString, "eighty");
		elseif(tensPlace == 7)
			numberString = strcat(numberString, "seventy");
		elseif(tensPlace == 6)
			numberString = strcat(numberString, "sixty");
		elseif(tensPlace == 5)
			numberString = strcat(numberString, "fifty");
		elseif(tensPlace == 4)
			numberString = strcat(numberString, "forty");
		elseif(tensPlace == 3)
			numberString = strcat(numberString, "thirty");
		elseif(tensPlace == 2)
			numberString = strcat(numberString, "twenty");
		end
		number -= (tensPlace * 10);
		%If there is something left in the number you will need a space to separate it
		if(number > 0)
			numberString = strcat(numberString, ' ');
		end
	%Check for teens
	elseif((number / 10) >= 1)
		onesPlace = mod(number, 10);
		if(onesPlace == 9)
			numberString = strcat(numberString, "nineteen");
		elseif(onesPlace == 8)
			numberString = strcat(numberString, "eighteen");
		elseif(onesPlace == 7)
			numberString = strcat(numberString, "seventeen");
		elseif(onesPlace == 6)
			numberString = strcat(numberString, "sixteen");
		elseif(onesPlace == 5)
			numberString = strcat(numberString, "fifteen");
		elseif(onesPlace == 4)
			numberString = strcat(numberString, "fourteen");
		elseif(onesPlace == 3)
			numberString = strcat(numberString, "thirteen");
		elseif(onesPlace == 2)
			numberString = strcat(numberString, "twelve");
		elseif(onesPlace == 1)
			numberString = strcat(numberString, "eleven");
		elseif(onesPlace == 0)
			numberString = strcat(numberString, "ten");
		end
		%If this if was hit number was used up
		number = 0;
	end

	%Check for ones place
	if(number >= 1)
		if(number == 9)
			numberString = strcat(numberString, "nine");
		elseif(number == 8)
			numberString = strcat(numberString, "eight");
		elseif(number == 7)
			numberString = strcat(numberString, "seven");
		elseif(number == 6)
			numberString = strcat(numberString, "six");
		elseif(number == 5)
			numberString = strcat(numberString, "five");
		elseif(number == 4)
			numberString = strcat(numberString, "four");
		elseif(number == 3)
			numberString = strcat(numberString, "three");
		elseif(number == 2)
			numberString = strcat(numberString, "two");
		elseif(number == 1)
			numberString = strcat(numberString, "one");
		end
		%If this if was hit number was used up
		number = 0;
	end
end

%{
Results:
The sum of all printed number from 1-1000 is 21124
It took 2.79162 seconds to run this algorithm
%}
