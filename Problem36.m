function [] = Problem36()
%ProjectEuler/ProjectEulerOctave/Problem36.m
%Matthew Ellison
% Created: 06-29-21
%Modified: 06-29-21
%Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
%{
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}

%Setup the variables
MAX_NUM = 999999;	%The largest number that will be checked
palindromes = [];	%All numbers that are palindromes in base 10 and 2
sumOfPal = 0;	%The sum of all elements in the list of palindromes

%Start the timer
startTime = clock();

%Start with 1, check if it is a palindrome in base 10 and 2, and continue to MAX_NUM
for num = 1 : MAX_NUM
	%Check if num is a palindrome
	if(isPalindrome(num2str(num)))
		%Convert num to base 2 and see if that is a palindrome
		binNum = toBin(num);
		if(isPalindrome(binNum))
			%Add num to the list of palindromes
			palindromes(end + 1) = num;
		end
	end
end
%Get the sum of all palindromes in the list
sumOfPal = sum(palindromes);

%Stop the timer
endTime = clock();

%Print the results
printf("The sum of all base 10 and base 2 palindromic numbers < %d is %d\n", MAX_NUM, sumOfPal);
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

function [revStr] = reverse(str)
	counter = size(str)(2);	%Set the counter to the last element in string
	%Loop until the counter reaches 0
	while(counter > 0)
		%Add the current element of string to rString
		revStr(end + 1) = str(counter);
		--counter;
	end
end

function [isPal] = isPalindrome(str)
	rev = reverse(str);
	if(str == rev)
		isPal = true;
	else
		isPal = false;
	end
end

function [binStr] = toBin(num)
	binStr = dec2bin(num);
end

%{
Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took 630.539528 seconds to run this algorithm
%}
