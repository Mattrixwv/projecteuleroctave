function [] = Problem12()
%ProjectEuler/Octave/Problem12.m
%Matthew Ellison
% Created: 
%Modified: 03-28-19
%What is the value of the first triangle number to have over five hundred divisors?
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


%Setup your variables
number = 1;		%To hold the current triangle number
nextNumber = 2;	%To hold the current number you have counted up to
found = false;	%To tell when the answer has been found
numDivisors = 0;

%Start the timer
startTime = clock();

%Keep checking to find the correct numbers until you 
while((~found) && (number > 0))
	%See how many divisors the number has
	numDivisors = size(getDivisors(number))(2);

	%If it is enough set the flag to stop the loop
	if(numDivisors > 500)
		found = true;
	else
		number += nextNumber;
		nextNumber += 1;
	end
end

%Stop the timer
endTime = clock();

%Print your result
printf("The first triangular number with more than 500 divisors is %d\n", number)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end %End of Problem12()

function [divisors] = getDivisors(goalNumber)
	divisors = [];
	%Start by checking that the number is positive
	if(goalNumber <= 0)
		return;
	%If the number is 1 return just itself
	elseif(goalNumber == 1)
		divisors(end + 1) = 1;
		return;
	%Otherwise add 1 and itself to the list
	else
		divisors(end + 1) = 1;
		divisors(end + 1) = goalNumber;
	end

	%Start at 3 and loop through all numbers < sqrt(goalNumber) looking for a number that divides it evenly
	topPossibleDivisor = ceil(sqrt(goalNumber));
	for possibleDivisor = 2 : topPossibleDivisor
		%If you find one add it and the number it creates to the list
		if(mod(goalNumber, possibleDivisor) == 0)
			divisors(end + 1) = possibleDivisor;
			%Account for the possibility sqrt(goalNumber) being a divisor
			if(possibleDivisor != topPossibleDivisor)
				divisors(end + 1) = goalNumber / possibleDivisor;
			end
		end
	end

	%Sort the list before returning for neatness
	divisors = sort(divisors);
end

%{
Results:
The first triangular number with more than 500 divisors is 76576500
It took 344.606964 seconds to run this algorithm
%}
