function [] = Problem33()
%ProjectEuler/Octave/Problem33.m
%Matthew Ellison
% Created: 02-07-21
%Modified: 02-07-21
%{
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
%}
%{
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}

%Setup the variables
MIN_NUMERATOR = 10;
MAX_NUMERATOR = 98;
MIN_DENOMINATOR = 11;
MAX_DENOMINATOR = 99;
NUMERATORS = [];
DENOMINATORS = [];
prodDenominator = 1;

%Start the timer
startTime = clock();

%Search every possible numerator/denominator pair
for denominator = MIN_DENOMINATOR : MAX_DENOMINATOR
	for numerator = MIN_NUMERATOR : (denominator - 1)
		num = num2str(numerator);
		denom = num2str(denominator);
		tempNum = 0;
		tempDenom = 0;

		%Check that this isn't a trivial example
		if((num(2) == '0') && (denom(2) == '0'))
			continue;
		elseif(num(1) == denom(1))
			tempNum = str2num(num(2));
			tempDenom = str2num(denom(2));
		elseif(num(1) == denom(2))
			tempNum = str2num(num(2));
			tempDenom = str2num(denom(1));
		elseif(num(2) == denom(1))
			tempNum = str2num(num(1));
			tempDenom = str2num(denom(2));
		elseif(num(2) == denom(2))
			tempNum = str2num(num(1));
			tempDenom = str2num(denom(1));
		end
		if(tempDenom == 0)
			continue;
		end

		%Test if the new fraction is the same as the old one
		if((tempNum / tempDenom) == (numerator / denominator))
			numerators(end + 1) = numerator;
			denominators(end + 1) = denominator;
		end
	end
end

%Get the product of the numbers
numProd = prod(numerators);
denomProd = prod(denominators);
%Get the gcd to reduce to lowest terms
GCD = gcd(numProd, denomProd);
%Save the denominator
prodDenominator = denomProd / GCD;


%Stop the timer
endTime = clock();

%Print the results
printf("The denominator of the product is %d\n", prodDenominator)
printf("It took %f seconds to run this algorithm\n", etime(endTime, startTime))

end

%{
Results:
The denominator of the product is 100
It took 4.728714 seconds to run this algorithm
%}
