function [rString] = Reverse(str)
%Reverse(string)
%This function Reverse the order of the elements in an array
%It was specifically designed for a string, but should work on other 1xX arrays

%ProjectEuler/Octave/Reverse.m
%Matthew Ellison
% Created:
%Modified: 03-28-19
%This is a function that reverses the elements in an array

	if(nargin ~= 1)
		error('That is not a valid number of arguments')
		return;
	end

	counter = size(str)(2);	%Set the counter to the last element in string
	%Loop until the counter reaches 0
	while(counter > 0)
		%Add the current element of string to rString
		rString(end + 1) = str(counter);
		--counter;
	end
end
