function [] = Problem19()
%ProjectEuler/Octave/Problem19.m
%Matthew Ellison
% Created: 03-13-19
%Modified: 03-28-19
%How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
%{
Results:
You are given the following information, but you may prefer to do some research for yourself.
1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
%}
%All of my requires, unless otherwise listed, can be found at https://bitbucket.org/Mattrixwv/luaClasses
%{
	Copyright (C) 2019  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
%}


global DAYS = [];
DAYS.SUNDAY = 0;
DAYS.MONDAY = 1;
DAYS.TUESDAY = 2;
DAYS.WEDNESDAY = 3;
DAYS.THURSDAY = 4;
DAYS.FRIDAY = 5;
DAYS.SATURDAY = 6;
DAYS.NUMBER_OF_DAYS = 7;
DAYS.ERROR = 8;
START_YEAR = 1901;
END_YEAR = 2000;


%Star the timer
startTime = clock();

%Setup the variables
totalSundays = 0;

%Run for all years from start to end
for year=START_YEAR : END_YEAR
	%Run for all months in the year
	for month=1 : 12
		day = getDay(month, 1, year);
		if(day == DAYS.ERROR)
			disp("There was an error with the day\n")
			return;
		elseif(day == DAYS.SUNDAY)
			totalSundays = totalSundays + 1;
		end
	end
end

%Stop the timer
stopTime = clock();

%Print the results
printf("There are %d Sundays that landed on the first of the month from %d to %d\n", totalSundays, START_YEAR, END_YEAR)
printf("It took %f to run this algorithm\n", etime(stopTime, startTime))

end %Problem19()


%Returns the day of the week that the day you pass into it is on
function [day] = getDay(month, day, year)
	global DAYS;
	%Make sure the numbers are within propper bounds
	if((month < 1) || (month > 12) || (day < 1) || (day > 31) || (year < 1))
		day = DAYS.ERROR;
		return;
	end

	numDays = 0;
	currentYear = 1;
	currentMonth = 1;
	currentDay = DAYS.SATURDAY;
	day = day - 1;

	%Add the correct number of days for every year
	while(currentYear < year)
		if(isLeapYear(currentYear))
			numDays = numDays + 366;
		else
			numDays = numDays + 365;
		end
		currentYear = currentYear + 1;
	end
	%Add the correct number of days for every month
	while(currentMonth < month)
		%February
		if(currentMonth == 2)
			if(isLeapYear(currentYear))
				numDays = numDays + 29;
			else
				numDays = numDays + 28;
			end
		%31 day months
		elseif((currentMonth == 1) || (currentMonth == 3) || (currentMonth == 5) || (currentMonth == 7) || (currentMonth == 8) || (currentMonth == 10) || (currentMonth == 12))
			numDays = numDays + 31;
		%30 day months
		else
			numDays = numDays + 30;
		end
		currentMonth = currentMonth + 1;
	end
	%Account for the weird year of 1752
	if(year > 1752)
		numDays = numDays - 11;
	elseif(year == 1752)
		if(month > 9)
			numDays = numDays - 1;
		elseif(month == 9)
			if(day >= 14)
				numDays = numDays - 11;
			%Days 3-13 were skipped that year
			elseif((day > 2) && (day < 14))
				day = ERROR;
				return;
			end
		end
	end
	%Add the correct number of days for every day
	numDays = numDays + day;

	currentDay = currentDay + numDays;
	currentDay = mod(currentDay, DAYS.NUMBER_OF_DAYS);
	if(currentDay == DAYS.SUNDAY)
		day = DAYS.SUNDAY;
	elseif(currentDay == DAYS.MONDAY)
		day = DAYS.MONDAY;
	elseif(currentDay == DAYS.TUESDAY)
		day = DAYS.TUESDAY;
	elseif(currentDay == DAYS.WEDNESDAY)
		day = DAYS.WEDNESDAY;
	elseif(currentDay == DAYS.THURSDAY)
		day = DAYS.THURSDAY;
	elseif(currentDay == DAYS.FRIDAY)
		day = DAYS.FRIDAY;
	elseif(currentDay == DAYS.SATURDAY)
		day = DAYS.SATURDAY;
	else
		day = DAYS.ERROR;
	end
end

function [answer] = isLeapYear(year)
	if(year < 1)
		answer = false;
	elseif(mod(year, 100) == 0)
		%This rule only applies at and after 1800
		if(year <= 1700)
			answer = true;
		elseif(mod(year, 400) == 0)
			answer = true;
		else
			answer = false;
		end
	elseif(mod(year, 4) == 0)
		answer = true;
	else
		answer = false;
	end
end

%{
There are 171 Sundays that landed on the first of the month from 1901 to 2000
It took 68.439590 to run this algorithm
%}
